import { filterInputParser } from './parsers/advancedFilterParser';
import {
  FormInputToRelations,
  isMetaDataRelation,
  MediaFileToMedia,
  parseIdToGetMoreData,
} from './parsers/entity';
import {
  resolveMedia,
  resolveMetadata,
  resolvePermission,
} from './resolvers/entityResolver';
import {
  resolveEntityFilters,
  resolveMediafileFilters,
  resolveSourceOptions,
} from './resolvers/filterResolver';
import { resolveMediafileForm } from './resolvers/formResolver';
import { collectieOptions, typeOptions } from './sources/filters';
import {
  boxEntityForm,
  FrameForm,
  StoryForm,
  AssetForm,
} from './sources/forms';
import {
  Collection,
  EntitiesResults,
  Form,
  JobType,
  Maybe,
  Resolvers,
  SearchInputType,
} from './type-defs';
const GraphQLUpload = require('graphql-upload/GraphQLUpload.js');
import { Context } from './types';

export const resolvers: Resolvers<Context> = {
  Upload: GraphQLUpload,
  Query: {
    Entity: async (_source, { id, type }, { dataSources }) => {
      if (type === 'MediaFile') {
        return await dataSources.CollectionAPI.getMediaFile(id);
      } else {
        return dataSources.CollectionAPI.getEntity(parseIdToGetMoreData(id));
      }
    },
    Entities: async (
      _source,
      { limit, skip, searchValue, advancedSearchValue, searchInputType },
      { dataSources }
    ) => {
      let entities: EntitiesResults = { results: [] };
      if (searchInputType === SearchInputType.AdvancedSavedSearchType) {
        entities = await dataSources.CollectionAPI.getSavedSearches(
          limit || 20,
          skip || 0,
          searchValue
        );
      } else if (searchInputType === SearchInputType.AdvancedInputMediaFilesType) {
        entities = await dataSources.SearchAPI.getAdvancedMediaFiles(
          limit || 20,
          skip || 0,
          advancedSearchValue ? filterInputParser(advancedSearchValue) : []
        );
      } else if (searchInputType === SearchInputType.AdvancedInputType) {
        entities = await dataSources.SearchAPI.GetAdvancedEntities(
          limit || 20,
          skip || 0,
          advancedSearchValue ? filterInputParser(advancedSearchValue) : []
        );
      } else {
        entities = await dataSources.SearchAPI.getEntities(
          limit || 20,
          skip || 0,
          searchValue || { value: '' }
        );
      }
      return entities;
    },
    Directories: async (_source, { dir }, { dataSources }) => {
      return dataSources.ImportAPI.getDirectories(dir || '');
    },
    Jobs: async (
      _source,
      { paginationInfo, filters, failed },
      { dataSources }
    ) => {
      return dataSources.CollectionAPI.getJobs(
        paginationInfo || { limit: 5, skip: 0 },
        filters || { query: '', type: '' as JobType },
        failed
      );
    },
    Job: async (_source, { id, failed }, { dataSources }) => {
      return dataSources.CollectionAPI.getJob(id, failed);
    },
    advancedFilters: async (_source, { choice }, { dataSources }) => {
      if (choice === 'mediaFileFilters') {
        return await resolveMediafileFilters(dataSources);
      } else {
        return await resolveEntityFilters(dataSources);
      }
    },
    FilterOptions: async (_source, { key }, { dataSources }) => {
      switch (key) {
        case 'museum' || 'source':
          return await resolveSourceOptions(dataSources);
        case 'getty':
          return collectieOptions;
        case 'type':
          return typeOptions;
        default:
          return null;
      }
    },
    Form: async (_source, { type }, { dataSources }): Promise<Maybe<Form>> => {
      switch (type) {
        case 'frame':
          return FrameForm;
        case 'story':
          return StoryForm;
        case 'box':
          return boxEntityForm;
        case 'media':
          return await resolveMediafileForm(dataSources);
        default:
          return null;
      }
    },
    UserPermissions: async (_source, _args, { dataSources }) => {
      return dataSources.CollectionAPI.getUserPermissions();
    },
  },
  Mutation: {
    deleteRelations: async (_source, { id, metadata }, { dataSources }) => {
      return dataSources.CollectionAPI.deleteRelations(id, metadata);
    },
    getAssetsRelationedWithMediafFile: async (
      _source,
      { mediaFileId },
      { dataSources }
    ) => {
      return dataSources.CollectionAPI.getAssetsRelationedWithMediafFile(
        mediaFileId
      );
    },
    replaceMetadata: async (_source, { id, metadata }, { dataSources }) => {
      return dataSources.CollectionAPI.replaceMetadata(id, metadata);
    },
    setMediaPrimaire: async (
      _source,
      { entity_id, mediafile_id },
      { dataSources }
    ) => {
      return dataSources.CollectionAPI.setMediaPrimaire(
        entity_id,
        mediafile_id
      );
    },
    setThumbnailPrimaire: async (
      _source,
      { entity_id, mediafile_id },
      { dataSources }
    ) => {
      return dataSources.CollectionAPI.setThumbnailPrimaire(
        entity_id,
        mediafile_id
      );
    },
    updateMediafilesOrder: async (_source, { value }, { dataSources }) => {
      return dataSources.CollectionAPI.updateMediafilesOrder(value);
    },
    postMediaFile: async (
      _source,
      { mediaFileInput, file },
      { dataSources }
    ) => {
      const mediaFileResult: any =
        await dataSources.CollectionAPI.postMediaFile(mediaFileInput);
      await dataSources.StorageAPI.uploadFile(mediaFileResult._key, file);
      const mediaFileById = await dataSources.CollectionAPI.getMediaFile(
        mediaFileResult._key
      );
      return mediaFileById;
    },
    linkMediafileToEntity: async (
      _source,
      { entityId, mediaFileInput },
      { dataSources }
    ) => {
      const linkedResult: any =
        await dataSources.CollectionAPI.linkMediafileToEntity(
          entityId,
          mediaFileInput
        );
      return linkedResult;
    },
    patchMediaFileMetadata: async (
      _source,
      { MediafileId, MediaFileMetadata },
      { dataSources }
    ) => {
      return dataSources.CollectionAPI.patchMetaDataMediaFile(
        MediafileId,
        MediaFileMetadata
      );
    },
    StartImport: async (_source, { folder }, { dataSources }) => {
      return dataSources.ImportAPI.startImport(folder);
    },
    // addRelations: async (_source, { id, relations }, { dataSources }) => {
    //   return dataSources.CollectionAPI.addRelations(id, relations);
    // }
    replaceRelationsAndMetaData: async (
      _source,
      { id, form },
      { dataSources }
    ) => {
      const relationInput = FormInputToRelations(form);

      if (relationInput && relationInput.length > 0)
        await dataSources.CollectionAPI.patchRelations(id, relationInput);

      if (form?.Metadata)
        await dataSources.CollectionAPI.patchMetadata(id, form.Metadata);

      return dataSources.CollectionAPI.getEntity(parseIdToGetMoreData(id));
    },
    deleteData: async (_source, { id, path }, { dataSources }) => {
      return dataSources.CollectionAPI.deleteData(id, path);
    },
    createEntity: async (_source, { entity }, { dataSources }) => {
      const createdEntity = await dataSources.CollectionAPI.createEntity(
        entity
      );
      if (createdEntity._id && entity.type === 'asset') {
        const id = createdEntity._id.replace('entities/', '');
        const objectId = await dataSources.CollectionAPI.getSixthCollectionId();
        await dataSources.CollectionAPI.patchRelations(id, [
          {
            key: objectId,
            label: 'MaterieelDing.beheerder',
            value: 'sixth_collection',
            type: 'isIn',
          },
        ]);
        await dataSources.CollectionAPI.patchMetadata(id, [
          {
            key: 'object_number',
            value: objectId.replace('cogent:', ''),
            lang: 'nl',
            label: 'objectnummer',
            immutable: true,
          },
        ]);
      }

      return createdEntity;
    },
    savedSearches:  async (_source, {}, { dataSources }) => {
      return dataSources.CollectionAPI.getSavedSearches(5, 1, { value: '', isAsc: false, key: '' });
    },
    createSavedSearch: async (_source, { savedSearchInput }, { dataSources }) => {
      return dataSources.CollectionAPI.createSavedSearch(savedSearchInput);
    },
    deleteSavedSearch: async (_source, { uuid }, { dataSources }) => {
      return dataSources.CollectionAPI.deleteSavedSearch(uuid);
    },
    patchSavedSearchTitle: async (_source, { uuid, title }, { dataSources }) => {
      return dataSources.CollectionAPI.patchSavedSearchTitle(uuid, title);
    },
    patchSavedSearchDefinition: async (_source, { uuid, definition }, { dataSources }) => {
      return dataSources.CollectionAPI.patchSavedSearchDefinition(uuid, definition);
    },
    getSavedSearchById: async (_source, { uuid }, { dataSources }) => {
      return dataSources.CollectionAPI.getSavedSearchById(uuid);
    },
  },
  Entity: {
    __resolveType(obj: any) {
      if (obj.type === 'asset') {
        return 'Asset';
      }
      if (obj.type === 'frame') {
        return 'Frame';
      }
      if (obj.type === 'story') {
        return 'Story';
      }
      if (obj.type === 'box') {
        return 'boxEntity';
      }
      if (obj.type === 'testimony') {
        return 'Testimony';
      }
      if (obj.type === 'person') {
        return 'person';
      }
      if (
        ['thesaurus', 'getty', 'role', 'gender', 'museum'].includes(obj.type)
      ) {
        return 'SimpleEntity';
      }
      if (
        [
          'classification',
          'redirected_by',
          'is_part_of',
          'wears',
          'has_creation',
          'production',
          'death',
          'aquisition',
          'consists_of',
        ].includes(obj.type)
      ) {
        return 'IntermediateEntity';
      }
      if (obj.type === 'saved_search') {
        return 'SavedSearchedEntity'
      }
      if (obj.type === 'MediaFile' || obj.filename) {
        return 'MediaFileEntity';
      }
      return 'BaseEntity';
    },
  },
  MediaFile: {
    isPublic: async (parent: any, _args, { dataSources }) => {
      let result = false;
      try {
        if (
          parent.metadata.filter(
            (metaDataItem: { key: string }) =>
              metaDataItem.key === 'publication_status'
          )[0]
        ) {
          result =
            parent.metadata.filter(
              (metaDataItem: { key: string }) =>
                metaDataItem.key === 'publication_status'
            )[0].value === 'publiek';
        } else {
          result = false;
        }
      } catch (e) {
        console.log(e);
      }
      return result;
    },
  },
  BaseEntity: {
    media: async (parent: any, _args, { dataSources }) => {
      return resolveMedia(dataSources, parent);
    },
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      return await resolveMetadata(parent, keys, excludeOrInclude);
    },
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(dataSources, parent.id);
    },
  },
  Asset: {
    media: async (parent: any, _args, { dataSources }) => {
      return resolveMedia(dataSources, parent);
    },
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      return await resolveMetadata(parent, keys, excludeOrInclude);
    },
    form: async (parent, _args, { dataSources }) => {
      let find6thCollection = false;
      const sixthCollectionId =
        await dataSources.CollectionAPI.getSixthCollectionId();
      parent.metadata.forEach((metadata) => {
        if (metadata?.key === sixthCollectionId) {
          find6thCollection = true;
        }
      });
      return find6thCollection ? AssetForm : null;
    },
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(dataSources, parent.id);
    },
  },
  Frame: {
    media: async (parent: any, _args, { dataSources }) => {
      return resolveMedia(dataSources, parent);
    },
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      return await resolveMetadata(parent, keys, excludeOrInclude);
    },
    form: () => FrameForm,
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(dataSources, parent.id);
    },
  },
  Story: {
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      return await resolveMetadata(parent, keys, excludeOrInclude);
    },
    form: () => StoryForm,
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(dataSources, parent.id);
    },
  },
  Testimony: {
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      return await resolveMetadata(parent, keys, excludeOrInclude);
    },
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(dataSources, parent.id);
    },
  },
  boxEntity: {
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      return await resolveMetadata(parent, keys, excludeOrInclude);
    },
    form: () => boxEntityForm,
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(dataSources, parent.id);
    },
  },
  person: {
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      return await resolveMetadata(parent, keys, excludeOrInclude);
    },
    form: () => null,
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(dataSources, parent.id);
    },
  },
  SimpleEntity: {
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      return await resolveMetadata(parent, keys, excludeOrInclude);
    },
    form: () => null,
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(dataSources, parent.id);
    },
  },
  IntermediateEntity: {
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      return await resolveMetadata(parent, keys, excludeOrInclude);
    },
    form: () => null,
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(dataSources, parent.id);
    },
  },
  MediaFileEntity: {
    id: async (parent: any) => {
      return parent._key;
    },
    type: async (parent: any) => 'MediaFile',
    metadata: async (parent: any, { keys, excludeOrInclude }) => {
      const parseMetadata = await resolveMetadata(
        parent,
        keys,
        excludeOrInclude
      );
      parseMetadata.push({
        key: 'filename',
        label: 'filename',
        value: parent.filename,
      });
      return parseMetadata;
    },
    media: async (parent: any, _args) => {
      let parsedMedia = MediaFileToMedia(parent);
      return parsedMedia;
    },
    form: () => null,
    permission: async (parent: any, _args, { dataSources }) => {
      return resolvePermission(
        dataSources,
        parent['_id'].replace('mediafiles/', ''),
        Collection.Mediafiles
      );
    },
  },
  MetadataRelation: {
    linkedEntity: async (parent, _args, { dataSources }) => {
      return await dataSources.CollectionAPI.getEntity(
        parseIdToGetMoreData(parent.key)
      );
    },
  },
  MetadataAndRelation: {
    __resolveType(obj: any) {
      return isMetaDataRelation(obj);
    },
  },
  MetadataOrRelationField: {
    __resolveType(obj: any) {
      return obj.relationType ? 'RelationField' : 'MetadataField';
    },
  },
  Media: {
    mediafiles: async (parent: any, _args, { dataSources }) => {
      return parent.parentId
        ? dataSources.CollectionAPI.getMediafiles(parent.parentId)
        : parent.mediafiles;
    },
  },
};
