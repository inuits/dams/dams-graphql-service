import { AdvancedFilter, FilterOption } from "../type-defs";
import { DataSources } from "../types";

export const resolveEntityFilters = async (dataSources: DataSources): Promise<AdvancedFilter[]> => {
    return (await dataSources.CollectionAPI.getConfig()).filters.entityFilters as AdvancedFilter[];
}

export const resolveMediafileFilters = async (dataSources: DataSources): Promise<AdvancedFilter[]> => {
    return (await dataSources.CollectionAPI.getConfig()).filters.mediafileFilters as AdvancedFilter[];
}

export const resolveSourceOptions = async (dataSources: DataSources): Promise<FilterOption[]> => {
    return (await dataSources.CollectionAPI.getConfig()).filters.mediafileFilters[1].options as FilterOption[];
}