import {
  AdvancedFilter,
  AdvancedFilterTypes,
  FilterOption,
} from '../type-defs';


export const mediaFileFilters: AdvancedFilter[] = [
  {
    key: 'rights',
    label: 'Rights',
    type: AdvancedFilterTypes.Checklist,
  },
  {
    key: 'source',
    label: 'Source',
    type: AdvancedFilterTypes.Checklist,
  },
  {
    key: 'publication_status_media_file',
    label: 'Publication Status',
    type: AdvancedFilterTypes.Checklist,
  },
  {
    key: 'photographer',
    label: 'Photographer',
    type: AdvancedFilterTypes.Tekst,
  },
  {
    key: 'filename',
    label: 'Filename',
    type: AdvancedFilterTypes.Tekst,
  }
]

export const testFilters: AdvancedFilter[] = [
  {
    key: 'type',
    label: 'Type',
    type: AdvancedFilterTypes.Checklist,
  },
  {
    key: 'title',
    label: 'Titel',
    type: AdvancedFilterTypes.Tekst,
  },
  {
    key: 'object_number',
    label: 'Objectnummer',
    type: AdvancedFilterTypes.Tekst,
  },
  {
    key: 'description',
    label: 'Beschrijving',
    type: AdvancedFilterTypes.Tekst,
  },
  {
    key: 'material',
    label: 'Materiaal',
    type: AdvancedFilterTypes.Tekst,
  },
  {
    key: 'skos:note',
    label: 'Nota',
    type: AdvancedFilterTypes.Tekst,
  },
  // {
  //   key: 'getty',
  //   label: 'Collectie',
  //   type: AdvancedFilterTypes.Multiselect,
  // },
  {
    key: 'museum',
    label: 'Beheerder',
    type: AdvancedFilterTypes.Checklist,
  },
  {
    key: 'publication_status',
    label: 'Status',
    type: AdvancedFilterTypes.Checklist,
  },
  {
    key: 'mediafiles', 
    label: 'Aantal mediafiles', 
    type: AdvancedFilterTypes.Minmax,
    isRelation: true
  }
  // {
  //   key: 'hoogte',
  //   label: 'Hoogte',
  //   type: AdvancedFilterTypes.Minmax,
  // },
  // {
  //   key: 'breedte',
  //   label: 'Breedte',
  //   type: AdvancedFilterTypes.Minmax,
  // },
  // {
  //   key: 'diepte',
  //   label: 'Diepte',
  //   type: AdvancedFilterTypes.Minmax,
  // }
];

export const beheerderOptions: FilterOption[] = [
  {
    value: 'Archief Gent',
    label: 'Archief Gent',
  },
  {
    value: 'Design Museum Gent',
    label: 'Design Museum Gent',
  },
  {
    value: 'Industriemuseum',
    label: 'Industriemuseum',
  },
  {
    value: 'STAM - Stadsmuseum Gent',
    label: 'STAM - Stadsmuseum Gent',
  },
  {
    value: 'Huis van Alijn',
    label: 'Huis van Alijn',
  },
  {
    value: 'De Zesde Collectie',
    label: 'De Zesde Collectie',
  },
];
export const collectieOptions: FilterOption[] = [
  {
    value: 'huishoudelijke apparaten',
    label: 'huishoudelijke apparaten',
  },
  {
    value: 'Bouwaanvragen, 1671-1795 (OA_535)',
    label: 'Bouwaanvragen, 1671-1795 (OA_535)',
  },
];

export const statusOptions: FilterOption[] = [
  {
    label: 'Gepubliceerd',
    value: 'publiek',
  },
  {
    label: 'In behandeling',
    value: 'te valideren',
  },
  {
    label: 'Afgekeurd',
    value: 'afgekeurd',
  },
];

export const statusOptionsMediaFile: FilterOption[] = [
  {
    label: 'Publiek',
    value: 'publiek',
  },
  {
    label: 'Niet publiek',
    value: 'niet-publiek',
  },
  {
    label: 'Werkbeeld',
    value: 'werkbeeld',
  },
  {
    label: 'Kandidaat',
    value: 'kandidaat',
  },
  {
    label: 'Beschermd',
    value: 'beschermd',
  },
  {
    label: 'Expliciet',
    value: 'expliciet',
  },
];

export const typeOptions: FilterOption[] = [
  {
    value: 'asset',
    label: 'Asset',
  },
  {
    value: 'story',
    label: 'Story',
  },
  {
    value: 'frame',
    label: 'Frame',
  },
  {
    value: 'box',
    label: 'Box',
  },
  {
    value: 'testimony',
    label: 'Testimony',
  },
  {
    value: 'person',
    label: 'person',
  },
  {
    value: 'museum',
    label: 'museum',
  },
  {
    value: 'thesaurus',
    label: 'thesaurus',
  },
  {
    value: 'getty',
    label: 'getty',
  },
  {
    value: 'role',
    label: 'role',
  },
  {
    value: 'gender',
    label: 'gender',
  }
];

export const sourceOptions: FilterOption[] = [
  {
    label: 'Niets-Geselecteerd',
    value: 'niets-geselecteerd',
  },
  {
    label: 'Archief Gent',
    value: 'Archief Gent',
  },
  {
    label: 'Design Museum Gent',
    value: 'Design Museum Gent',
  },
  {
    label: 'Industriemuseum',
    value: 'Industriemuseum',
  },
  {
    label: 'STAM Gent',
    value: 'STAM Gent',
  },
  {
    label: 'Huis van Alijn',
    value: 'Huis van Alijn',
  },
  {
    label: 'De Zesde Collectie',
    value: 'sixth_collection',
  },
]

export const rightsOptions: FilterOption[] = [
  {
    label: 'Niets-Geselecteerd',
    value: 'niets-geselecteerd',
  },
  {
    label: 'Public Domain Mark 1.0',
    value: 'Public Domain Mark 1.0',
  },
  {
    label: 'CC0 1.0',
    value: 'CC0 1.0',
  },
  {
    label: 'CC BY-SA 4.0',
    value: 'CC BY-SA 4.0',
  },
  {
    label: 'CC BY-NC 4.0',
    value: 'CC BY-NC 4.0',
  },
  {
    label: 'CC BY-NC-ND 4.0',
    value: 'CC BY-NC-ND 4.0',
  },
  {
    label: 'In Copyright',
    value: 'In Copyright',
  },
  {
    label: 'In Copyright - non-commercial use permitted',
    value: 'In Copyright - non-commercial use permitted',
  },
  {
    label: 'In Copyright - unknown rightsholder',
    value: 'In Copyright - unknown rightsholder',
  },
  {
    label: 'Copyright Undetermined',
    value: 'Copyright Undetermined',
  },
];
