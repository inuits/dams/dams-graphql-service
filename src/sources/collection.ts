import {
  Entity,
  Metadata,
  MediaFile,
  PaginationInfo,
  JobsResults,
  Filters,
  Job,
  MetadataFieldInput,
  Maybe,
  Collection,
  EntityInput,
  MediaFileInput,
  MediaFileMetadataInput,
  OrderArrayInput,
  MetadataInput,
  SavedSearchesResults,
  SavedSearchInput,
  SavedSearchedEntity,
  SearchFilter,
  EntitiesResults,
  FilterInput
} from '../type-defs';
import { AuthRESTDataSource } from 'inuits-apollo-server-auth';

import { Context, Config } from '../types';
import { setId } from '../parsers/entity';
import { environment as env } from '../environment';
import { addCustomMetadataToEntity } from '../resolvers/entityResolver';
import { parsedInput } from '../parsers/advancedFilterParser';
export type relationInput = Record<string, string>[];
type updateNode = { id: String; order: number };
let sixthCollectionId: string | 'no-id' = 'no-id';

export class CollectionAPI extends AuthRESTDataSource<Context> {
  public baseURL = `${env.api.collectionApiUrl}/`;
  public config: Config | 'no-config' = 'no-config';

  async getUserPermissions(): Promise<{ payload: string[] }> {
    let data: string[] = await this.get<string[]>('user/permissions');
    return { payload: data };
  }

  async getEntity(id: string): Promise<any> {
    let data = await this.get<any>(id);
    try {
      setId(data);
      const ldesResource = data.data['foaf:page'];
      const iiifPresentation =
        data.data['Entiteit.isHetOnderwerpVan'][0]['@id'];
      addCustomMetadataToEntity(data, [
        { key: 'ldesResource', value: ldesResource, label: 'Bron' },
        {
          key: 'iiifPresentation',
          value: iiifPresentation,
          label: 'Presentatie',
        },
      ]);
    } catch (e) {
      console.log(e);
    }
    return data;
  }

  async setMediaPrimaire(
    entity_id: string,
    mediafile_id: string
  ): Promise<any> {
    const data = await this.put<any>(
      `entities/${entity_id}/set_primary_mediafile/${mediafile_id}`
    );
    return data;
  }

  async setThumbnailPrimaire(
    entity_id: string,
    mediafile_id: string
  ): Promise<any> {
    const data = await this.put<any>(
      `entities/${entity_id}/set_primary_thumbnail/${mediafile_id}`
    );
    return data;
  }

  async getMediafiles(id: string): Promise<MediaFile[]> {
    if (id !== 'noid') {
      return await this.get(`entities/${id}/mediafiles`, {
        non_public: '1',
      });
    } else {
      return [];
    }
  }

  async addRelations(id: string, relations: any[]): Promise<any[]> {
    relations.map((relation) => (relation.key = 'entities/' + relation.key));
    return await this.post(`entities/${id}/relations`, relations);
  }

  async getMediaFile(mediaFileId: String): Promise<any> {
    const res = await this.get(`/mediafiles/${mediaFileId}`);
    return res;
  }

  async getAssetsRelationedWithMediafFile(mediaFileId: String): Promise<any> {
    const assets = await this.get(`/mediafiles/${mediaFileId}/assets`);
    assets.forEach((asset: any) => {
      setId(asset);
    });
    return assets;
  }

  async postMediaFile(mediaFileInput: MediaFileInput): Promise<any> {
    mediaFileInput.metadata = [
      { key: 'rights', value: 'niets-geselecteerd' },
      { key: 'source', value: 'niets-geselecteerd' },
      { key: 'publication_status', value: 'niet-publiek' },
    ];
    const res = await this.post(`/mediafiles`, mediaFileInput);
    return res;
  }

  async linkMediafileToEntity(
    entityId: String,
    mediaFileInput: MediaFileInput
  ): Promise<any> {
    const res = await this.post(
      `/entities/${entityId}/mediafiles`,
      mediaFileInput
    );
    return res;
  }

  async patchMetaDataMediaFile(
    mediafileId: String,
    mediaFileMetadata: Maybe<MediaFileMetadataInput>[]
  ): Promise<any> {
    return await this.patch(`/mediafiles/${mediafileId}`, {
      metadata: mediaFileMetadata,
    });
  }

  async patchRelations(id: string, relations: relationInput): Promise<any[]> {
    return await this.patch(`entities/${id}/relations`, relations);
  }

  async replaceMetadata(
    id: String,
    metadata: Maybe<MetadataFieldInput>[]
  ): Promise<Metadata[]> {
    return await this.put(`entities/${id}/metadata`, metadata);
  }

  async patchMetadata(
    id: String,
    metadata: Maybe<MetadataInput>[]
  ): Promise<Metadata[]> {
    return await this.patch(`entities/${id}/metadata`, metadata);
  }

  async getJobs(
    paginationInfo: PaginationInfo,
    filters: Filters,
    failed: Boolean
  ): Promise<JobsResults> {
    let url: string = `jobs?limit=${paginationInfo.limit}&skip=${paginationInfo.skip}${failed ? '&status=failed' : ''}`;
    if (filters.type) {
      url = `${url}&type=dams.${filters.type}`;
    }
    const data = await this.get(url);
    return data;
  }

  async getJob(id: String, failed: Boolean): Promise<Job> {
    const data = await this.get(`jobs/${id}?${failed ? '?status=failed' : ''}`);
    return data;
  }

  async deleteData(id: string, path: Collection): Promise<any> {
    if (id == null) {
      return 'no id was specified';
    } else {
      await this.delete(`${path}/${id}`);
      return 'data has been successfully deleted';
    }
  }

  async createEntity(entity: EntityInput): Promise<any> {
    const body = entity as Entity;
    const newEntity = await this.post(`entities`, {
      type: body.type,
      metadata: [
        {
          key: 'title',
          value: entity.title,
        } as Metadata,
      ],
    });
    setId(newEntity);
    return setId(newEntity);
  }

  async updateMediafilesOrder(orderArray: OrderArrayInput): Promise<string> {
    let mfs: updateNode[] = Object.values(orderArray['value']);

    console.log(Object.values(orderArray['value']));
    for (let i = 0; i < mfs.length; i++) {
      let mf: updateNode = mfs[i];
      console.log(mf.id, mf.order);
      await this.patch(`/${mf.id}`, { order: mf.order });
    }
    return 'Successfuly changed mediafile order';
  }

  async getPermission(id: string, collection: Collection): Promise<string[]> {
    return this.get(`${collection}/${id}/permissions`);
  }

  async getSixthCollectionId(): Promise<string> {
    if (sixthCollectionId == 'no-id') {
      sixthCollectionId = await this.get(`entities/sixthcollection/entity_id`);
    }
    return sixthCollectionId;
  }

  async deleteRelations(id: string, relations: any[]): Promise<string> {
    await this.delete(`entities/${id}/relations`, undefined, {
      body: JSON.stringify(relations),
      headers: { 'Content-Type': 'application/json' },
    });
    return 'Delete success.';
  }

  async getConfig(): Promise<Config> {
    if (this.config === 'no-config'){
      this.config = await this.get('/config');
    }
    return this.config as Config;
  }

  getSkip(skip: number, limit: number ) {
    return skip - 1 === 0 ? 0 : limit * (skip - 1)
  }

  setLabels = (response: any) => {
    response.metadata.forEach((md: Maybe<Metadata>) => {
      if (md) {
        md.label = md.key
      } 
    })
  }

  async getSavedSearches(
    limit: number = 5,
    skip: number = 0,
    searchValue: Maybe<SearchFilter> | undefined
  ): Promise<EntitiesResults> {
    if (!searchValue) {
      searchValue = { value: '', isAsc: false, key: '' }
    }
    var response = await this.get(`saved_searches?title=${searchValue.value}&limit=${limit}&skip=${this.getSkip(skip, limit)}&only_own=1`);
    response.results.forEach((res: SavedSearchedEntity) => {
      setId(res);
      this.setLabels(res);
    })
    return response
  }

  async createSavedSearch(savedSearchInput: SavedSearchInput): Promise<SavedSearchedEntity> {
    const response = await this.post(`saved_searches`,savedSearchInput);
    this.setLabels(response);
    return response
  }

  async deleteSavedSearch(uuid: String): Promise<string> {
    await this.delete(`saved_searches/${uuid}`);
    return `saved search with uuid ${uuid} has been deleted.`
  }

  async patchSavedSearchTitle(uuid: String, title: String): Promise<SavedSearchedEntity> {

    const patchedTitle = {
      metadata: [
        {
          lang: "nl",
          value: title,
          key: "title"
        }
      ]
    }

    const response = await this.patch(`saved_searches/${uuid}`, patchedTitle);
    this.setLabels(response);
    return response;
  }

  async patchSavedSearchDefinition(uuid: String, definition: FilterInput[]): Promise<SavedSearchedEntity> {
    const patchedDefinition = {
      definition: definition 
    }
    const response = await this.patch(`saved_searches/${uuid}`, patchedDefinition);
    this.setLabels(response);
    return response;
  }

  async getSavedSearchById(uuid: String): Promise<SavedSearchedEntity> {
    const response = await this.get(`saved_searches/${uuid}`);
    this.setLabels(response);
    return response;
  }

  

}
