import { EntitiesResults, SearchFilter } from '../type-defs';
import { setId, setType } from '../parsers/entity';
import { AuthRESTDataSource } from 'inuits-apollo-server-auth';
import { Context } from '../types';
import { environment as env } from '../environment';
import { parsedInput } from '../parsers/advancedFilterParser';

export class SearchAPI extends AuthRESTDataSource<Context> {
  public baseURL = `${env.api.searchApiUrl}/`;

  getSkip(skip: number, limit: number) {
    return skip - 1 === 0 ? 0 : limit * (skip - 1);
  }

  async getEntities(
    limit: number,
    skip: number,
    searchValue: SearchFilter
  ): Promise<EntitiesResults> {
    let data = [];
    try {
      let body = searchValue;
      data = await this.post(
        `search/collection?limit=${limit}&skip=${this.getSkip(skip, limit)}`,
        { ...body, relation_filter: [], skip_relations: true }
      );
      console.log({ data });
      data.results.forEach((element: any) => setId(element));
    } catch (e) {
      console.log(e);
    }
    return data;
  }

  async getAdvancedMediaFiles(
    limit: number,
    skip: number,
    advancedSearchValue: parsedInput[]
  ): Promise<EntitiesResults> {
    let result = { results: [], count: 0, limit };
    try {
      //Remove asset filter - first initial filter
      advancedSearchValue.shift();

      //mediaFileFilters - publication status
      if (advancedSearchValue) {
        advancedSearchValue.forEach((filter: parsedInput) => {
          if (
            filter &&
            filter.item_types?.includes('publication_status_media_file')
          ) {
            filter.item_types = ['publication_status'];
          }
        });
      }

      let body = advancedSearchValue;

      const data = await this.post(
        `advanced-search/mediafiles?limit=${limit}&skip=${this.getSkip(
          skip,
          limit
        )}`,
        body
      );
      if (data.results) {
        data.results?.forEach((element: any) => {
          setId(element);
          setType(element, 'MediaFile');
        });
        result = data;
      } else {
        data.forEach((element: any) => {
          setId(element);
          setType(element, 'MediaFile');
        });
        result = { results: data, count: data.length, limit: limit };
      }
    } catch (e) {
      console.log(e);
    }
    return result;
  }

  async GetAdvancedEntities(
    limit: number,
    skip: number,
    advancedSearchValue: parsedInput[]
  ): Promise<EntitiesResults> {
    let body = advancedSearchValue;
    const data = await this.post(
      `advanced-search?limit=${limit}&skip=${this.getSkip(skip, limit)}`,
      body
    );

    if (data.results) {
      data.results?.forEach((element: any) => setId(element));
      return data;
    } else {
      data.forEach((element: any) => setId(element));
      return { results: data, count: data.length, limit: limit };
    }
  }
}
