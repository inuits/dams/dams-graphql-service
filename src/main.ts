import { ApolloServer } from 'apollo-server-express';
import { readFileSync } from 'fs';
import express from 'express';
import cors from 'cors';
import { environment } from './environment';
import { CollectionAPI } from './sources/collection';
import { resolvers } from './resolvers';
import {
  applyAuthEndpoints,
  applyAuthSession,
  applyEnvironmentConfig,
} from 'inuits-apollo-server-auth';
import { SearchAPI } from './sources/search';
import { ImportAPI } from './sources/import';
import { StorageAPI } from './sources/storage';
import applyConfigEndpoint from './configEndpoint';
// @ts-ignore
import graphqlUploadExpress from 'graphql-upload/graphqlUploadExpress.js';
import applyMediaFileEndpoint from './sources/mediafiles';
import * as Sentry from '@sentry/node';

if (environment.sentryEnabled) {
  Sentry.init({
    dsn: environment.sentryDsn,
    sendClientReports: false,
    environment: environment.nomadNamespace,
  });
}

const startApolloServer = async () => {
  const app = express();
  const apolloServer = new ApolloServer({
    typeDefs: readFileSync('./schema.graphql').toString('utf-8'),
    resolvers,
    dataSources: () => ({
      CollectionAPI: new CollectionAPI(),
      SearchAPI: new SearchAPI(),
      ImportAPI: new ImportAPI(),
      StorageAPI: new StorageAPI(),
    }),
    context: ({ req, res }) => {
      return { session: req.session };
    },
    introspection: environment.apollo.introspection,
  });

  await apolloServer.start();

  app.use(express.json());
  app.use(
    cors({
      credentials: false,
      origin: [environment.damsFrontend],
    })
  );
  app.use(graphqlUploadExpress({ maxFileSize: 50000000, maxFiles: 20 }));
  applyConfigEndpoint(app, environment);
  applyEnvironmentConfig({
    tokenLogging: environment.apollo.tokenLogging,
    staticJWT: environment.staticToken,
  });
  applyAuthSession(app, environment.sessionSecret);

  apolloServer.applyMiddleware({
    app,
    path: environment.apollo.graphqlPath,
    cors: false,
  });

  applyAuthEndpoints(app, environment.oauth.baseUrl, environment.clientSecret);

  applyMediaFileEndpoint(
    app,
    environment.api.storageApiUrl,
    environment.api.iiifUrl,
    environment.staticToken
  );

  const httpServer = app.listen(environment.port, () => {
    console.log(`Server is running on port ${environment.port}`);
  });

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => {
      httpServer.close();
    });
  }

  return { apolloServer, app };
};

startApolloServer();
