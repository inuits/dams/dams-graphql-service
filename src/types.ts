import { CollectionAPI } from './sources/collection';
import { SearchAPI } from './sources/search';
import { ImportAPI } from './sources/import';
import { StorageAPI } from './sources/storage';
import {  AuthSessionResponse } from 'inuits-apollo-server-auth';
import { Cookie } from 'express-session';
import { FilterOption, Maybe, MetadataFieldOption } from './type-defs';
 

export interface DataSources {
  CollectionAPI: CollectionAPI;
  SearchAPI: SearchAPI;
  ImportAPI: ImportAPI;  
  StorageAPI: StorageAPI;
}

export interface Context {
  [x: string]: any;
  session: {
    auth: AuthSessionResponse | undefined, 
    cookie: Cookie
  }
  dataSources: DataSources;
}

export type filter = {
  key: string, 
  label: string, 
  type: string, 
  options?: Maybe<MetadataFieldOption>[],
  isRelation?: boolean
}

type Filters = {
  filters: Record<string, Array<filter>>
}

export type Config = Record<string, Array<string>> & Filters