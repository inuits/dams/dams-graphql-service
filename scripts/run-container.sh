#!/bin/bash
DOCKER=docker
if [ -x "$(command -v podman)" ]; then
  DOCKER=podman
fi

${DOCKER} run -it --rm -p ${1:4000}:4000 --env-file=.env.local inuits-dams-graphql-service:prod $@
